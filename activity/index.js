const express = require("express");

const mongoose = require("mongoose");

const app = express();


mongoose.connect("mongodb+srv://admin:admin@cluster0.rmv5r.mongodb.net/b153_tasks?retryWrites=true&w=majority", {
	useNewUrlParser: true,
	useUnifiedTopology: true
})

let db = mongoose.connection;
db.on("error", console.error.bind(console, "connection error"));

db.once("open",()=>console.log("We're now connected to MongoDB"));

app.use(express.json());

const port = 4000;

app.get("/", (req, res) => {
	res.send("Hello from Express!")
})


const userSchema = new mongoose.Schema({
	username: String,
	password: String
})

let User = mongoose.model("User",userSchema);

app.post('/',(req,res)=>{
let newTask = new User({
	username: req.body.username,
	password: req.body.password
})

// console.log(req.body);

newTask.save()
.then(result => res.send(result))
.catch(error => res.send(error))

})

app.get('/users', (req, res)=>{
User.find({})
.then(result => res.send(result))
.catch(error => res.send(error))
})

//start the server and confirm that it is running
app.listen(port, () => console.log(`Server running at port ${port}`))
