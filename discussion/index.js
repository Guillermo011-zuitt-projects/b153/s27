// import the contents of the express package to use for our application
const express = require("express");

// import mongoose package. Mongoose is an ODM (Object Document Mapper) library for MongoDB and Node.js that manges models/schemas and allows a quick and easy way to connect to a MongoDB database
const mongoose = require("mongoose");

// give the express() function from the express package a variable "app" so that it can be called more easily
const app = express();

// mongoose's connect method takes our MongoDB Atlas connection string and uses it to connect to Atlas and authenticate our credentials, as well as specifies the database that our app needs to use
// useNewURLParser and useUnifiedTopology are both set to true as part of a newer Mongoose update that allows a more efficent way to connect to ATlas, since the older way is about to be depreciated (become obsolete)

mongoose.connect("mongodb+srv://admin:admin@cluster0.rmv5r.mongodb.net/b153_tasks?retryWrites=true&w=majority", {
	useNewUrlParser: true,
	useUnifiedTopology: true
})

// confirm that Atlas connection is successful
// mongoose.connection.once('open', () => console.log("Now connected to MongoDB Atlas"))
// mongoose.connection is used to handle error or successful connection to MongoDB
let db = mongoose.connection;
// check mongoose.connection on error
// console.error.bind() will allow us to display the error of mongoose.connection both in your terminals and in our browser console.
db.on("error", console.error.bind(console, "connection error"));

// confirmation that connection to mongodb is successful
db.once("open", () => console.log("we're now connected to MongoDB"));

// middleware - are functions/methods we use in our server/api. The middleware functions serve as gates/it allows us to do other tasks:

// this will allow us to handle the request body json
// app.use(express.json());


// declare a port number variable
const port = 4000;

// create a GET route to check if Express is working
app.get("/", (req, res) => {
	res.send("Hello from Express!")
}) 

/*
	Mongoose Schema
- before we can create documents in our database we first have to declar a "blueprint or "template" of our documents. This is to ensure that the content fileds of documents are uniform.
- Schema acts a blueprint of our data/document. It is a representation of how our documents is structured. It also determines the types of data and the expected fields/properties per document.
*/

// Schema() is a constructor from mongoose that will allow us to create a new schema object.

const taskSchema = new mongoose.Schema({
// Define the fields for the task document. The task document should have a name and status field. Both fields must be strings.
name: String,
status: String
})
// // sample task
// {name: "Sample Task",
// status: "Complete"}

/*
	Mongoose Model
- models are used to connect your API to the corresponding collection in your database. It is a representationof your documents.
- models uses schemas to create objects/documents that corresponds to the schema. By default, when creating the collection from your model, the collection name is pluralized.
mongoose.model(<nameofCollection>, <schemaToFollow>)
*/

// code below will look like db.tasks in mongoDB 
let Task = mongoose.model("Task", taskSchema);

// POST Route - add task documents
app.post("/", (req, res)=>{
// check the incoming request body
	// console.log(req.body)

// create a new task object/document from our model
let newTask = new Task({
	name: req.body.name,
	status: req.body.status
})
// newTask is a mongoose document/object
// console.log(newTask);
// this method will allow us to save our document in our tasks collection
// .then() and catch() chain - .then() is used to handle the result/return of a function. If the method/function properly returns a value, we can run a separate function to handle it. .cathc90 is used to catch/handle an error.
newTask.save()
.then(result => res.send(result))
.catch(error => res.send(error))
})

app.get('/tasks', (req, res)=>{
// res.send("Testing from get all task documents route")
// to be able to find() or get all documents from a collection, we will use the find() method of our model.
// this would look like db.tasks.find({}) in mongoDB
Task.find({})
.then(result => res.send(result))
.catch(error => res.send(error))
})

// Activity s27

const userSchema = new mongoose.Schema({
	username: String,
	password: String
})

let User = mongoose.model("User",userSchema);

app.post('/',(req,res)=>{
let newTask = new User({
	username: req.body.username,
	password: req.body.password
})

// console.log(req.body);

newTask.save()
.then(result => res.send(result))
.catch(error => res.send(error))

})

app.get('/users', (req, res)=>{
User.find({})
.then(result => res.send(result))
.catch(error => res.send(error))
})

// start the server and confirm that it is running
app.listen(port, () => console.log(`Server running at port ${port}`))